# Learning Ada  

Last update: Aug 28 2019.  
Links:  

- [Introduction to Ada, from Learn Adacore.com](https://learn.adacore.com/courses/intro-to-ada/index.html)  


---  

## My follow-along source code  

### Imperative language  

- Hello, World: [greet.adb](_code/imperative_language/greet.adb), [greetUse.adb](_code/imperative_language/greetUse.adb)  
- The "`if/elsif/else`" statements: [check_positive_if.adb](_code/imperative_language/check_positive_if.adb), [check_positive_else.adb](_code/imperative_language/check_positive_else.adb), [check_direction.adb](_code/imperative_language/check_direction.adb).  
- Loops:  
  - [greet_5a.adb](_code/imperative_language/greet_5a.adb), [greet_5a_reverse.adb](_code/imperative_language/greet_5a_reverse.adb). Uses '`for`' loop to iterate over a discrete range of integers.  
  - [greet_5b.adb](_code/imperative_language/greet_5b.adb). Uses Ada's bare loop (foundation for other loops).  
  - [greet_5c.adb](_code/imperative_language/greet_5c.adb). Uses Ada's '`while'` loop.  
- '`case`' statement: [check_direction_case.adb](_code/imperative_language/check_direction_case.adb).  
- Declarative regions of a subprogram (between 'is' and 'begin'): [declarative_regions.adb](_code/imperative_language/declarative_regions.adb).  
  - Declaring a variable among statements (in new declarative region): [greet_declare_var.adb](_code/imperative_language/greet_declare_var.adb).  
- Conditional expressions:  
  - `if` expressions: [check_positive_if_expr.adb](_code/imperative_language/check_positive_if_expr.adb), [odd_or_even.adb](_code/imperative_language/odd_or_even.adb).  
  - `case` expressions: [odd_or_even2.adb](_code/imperative_language/odd_or_even2.adb).  


### Subprograms  


<br> 

---  

Currently on section: Subprograms start  

