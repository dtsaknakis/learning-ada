-- --------------------------------------------------------
-- Date created  : Aug 27 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Compile       : $ gprbuild -q -p learn.adb -gnatwa
-- Run           : $ ./learn
-- --------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;

procedure Learn is
    subtype Alphabet is Character range 'A' .. 'Z';

begin

    Put_Line ("Learning Ada from " & Alphabet'First & " to " & Alphabet'Last);

end Learn;
