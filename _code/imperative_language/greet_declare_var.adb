-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Greets a user in Ada2012.
--               : Declares a local variable amidst statements
--               : A declaration can't appear as a statement,
--               : so the local var is declared inside a new
--               : declarative region with a block statement.
-- Compile       : $ gprbuild greet_declare_var.adb
-- Run           : $ ./greet_declare_var
-- --------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO; -- module for input/output


procedure Greet is
begin
    loop
        Put_Line ("Please enter your name: ");

        -- Introduce new declarative region with block statement
        -- to declare a local 'Name' variable
        declare
            Name : String := Get_Line; -- Get_Line is like C's scanf
        begin
            exit when Name = "";
            Put_Line ("Hi " & Name & "!");
        end;

    -- Name is undefined here
    end loop;

    Put_Line ("Bye!");
end Greet;
