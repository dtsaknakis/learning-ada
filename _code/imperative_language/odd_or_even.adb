-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses an 'if' expression (note: NOT an if
--               : statement) in a for loop, in Ada2012.
--               : In 'if' expressions all branches must be
--               : of the same type; they must have ().
--               : They must contain an 'else' if the 'if' 
--               : is not returning a Boolean. Otherwise 
--               : the 'else' is optional.
-- Compile       : $ gprbuild odd_or_even.adb
-- Run           : $ ./odd_or_even
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;

procedure Odd_or_Even is
begin
    for I in 1 .. 10 loop
        Put_Line (if I mod 2 = 0 then "Even" else "Odd");
    end loop;
end Odd_or_Even;