-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Hello world in Ada2012, using a bare loop
--               : Bare loops are the foundation for other
--               : kinds of Ada loops.
-- Compile       : $ gprbuild greet_5b.adb
-- Run           : $ ./greet_5b
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;

procedure Greet_5b is
    -- Declarative region of the subprogram
    I : Integer := 1; -- variable declaration, initialization
begin
    -- Statement region of the subprogram
    loop
        Put_Line("Hello, World!" & Integer'Image(I));
        exit when I = 5; -- exit statement
        -- Assignment
        I := I + 1; -- no I++ short form in Ada for incrementing
    end loop;
end Greet_5b;