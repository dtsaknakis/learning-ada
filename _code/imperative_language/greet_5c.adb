-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Hello world in Ada2012, using a while loop
-- Compile       : $ gprbuild greet_5c.adb
-- Run           : $ ./greet_5c
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;

procedure Greet_5c is
    -- * Declarative region of the subprogram *
    I : Integer := 1; -- variable declaration, initialization
begin
    -- * Statement region of the subprogram *
    -- Condition must be a Boolean value (no Integers)
    -- Operator "<=" returns a Boolean
    while I <= 5 loop
        Put_Line ("Hello, World!" & Integer'Image (I));
        I := I + 1; -- increment I by 1
    end loop;
end Greet_5c;