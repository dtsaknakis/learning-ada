-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Demonstrates delcarative regions of a 
--               : subprogram (between 'is' and 'begin' in 
--               : Ada2012.
-- Compile       : $ gprbuild declarative_regions.adb
-- Run           : $ ./declarative_regions
-- --------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO; -- module for input/output

-- A procedure does not return a value when called.
procedure Main is
    -- Declarative region of Main subprogram
    procedure Nested is
    begin
        Put_Line ("Hello Dimitris");
    end Nested;
begin
    Nested;  -- call the Nested subprogram
end Main;