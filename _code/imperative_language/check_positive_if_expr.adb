-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses an 'if' expression (note: NOT an if
--               : statement) in Ada2012.
--               : In 'if' expressions all branches must be
--               : of the same type; they must have ().
-- Compile       : $ gprbuild check_positive_if_expr.adb
-- Run           : $ ./check_positive_if_expr
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO; -- integer I/O

procedure Check_Positive is
    N : Integer;  -- variable declaration
begin
    -- The Get() and Put() procedures are from Integer_Text_IO
    Put ("Enter an integer value: "); -- Put a String
    Get (N); -- Read an integer value (user's input)
    Put (N); -- Put an Integer
    declare
        S : String :=
            (if N > 0 then " is a positive number"
             else " is not a positive number");
    begin
        Put_Line (S);
    end;
end Check_Positive;