-- --------------------------------------------------------
-- Date created  : Aug 27 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : A hello world program in Ada2012. This 
--               : version includes the 'use' clause.
-- Compile       : $ gprbuild greetUse.adb
-- Run           : $ ./greetUse
-- --------------------------------------------------------

-- 'with' references external modules needed in the procedure;
-- The 'use' clause allows us to use the Put_Line() procedure
-- without prepending Ada.Text_IO to it
with Ada.Text_IO;  -- std lib module for input/output
use Ada.Text_IO;   -- use module's procedures directly

-- A procedure does not return a value when called. Greet is the
-- procedure name chosen.
procedure Greet is
begin
    -- Print "Hello, World!" to the screen; use Put_Line() procedure
    Put_Line ("Hello, World!");
end Greet;
