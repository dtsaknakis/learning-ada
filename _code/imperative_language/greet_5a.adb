-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Hello world in Ada2012, using a for loop
--               : to iterate over a discrete range of 
--               : numbers.
-- Compile       : $ gprbuild greet_5a.adb
-- Run           : $ ./greet_5a
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;

procedure Greet_5a is
begin
    -- Iterate over a discrete range of nrs 1-5 (5 inclusive)
    for I in 1..5 loop
        -- Integer'Image function converts Integer to String
        -- '&' concatenates string values
        Put_line ("Hello, World!" & Integer'Image (I)); -- I can be any name
    end loop;
end Greet_5a;