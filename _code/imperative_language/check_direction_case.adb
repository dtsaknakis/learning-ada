-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses an Ada2012 'case' statement.
-- Compile       : $ gprbuild check_direction_case.adb
-- Run           : $ ./check_direction_case
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO; -- integer I/O

procedure Check_Direction is
    N : Integer;  -- variable declaration
begin
    loop
        Put ("Enter an integer value: "); -- Put a String
        Get (N); -- Read an integer value (user's input)
        Put (N);  -- Put an Integer
        -- case statement; N must be a discrete type (int or enum)
        case N is
            when 0 | 360 =>
                Put_Line (" is due east");
            when 1 .. 89 => 
                Put_Line (" is in the northeast quadrant");
            when 90 =>
                Put_Line (" is due north");
            when 91 .. 179 =>
                Put_Line (" is in the northwest quadrant");
            when 180 =>
                Put_Line (" is due west");
            when 181 .. 269 =>
                Put_Line (" is in the southwest quadrant");
            when 270 =>
                Put_Line (" is due south");
            when 271 .. 359 =>
                Put_Line (" is in the southeast quadrant");
            when others => 
                Put_Line (" Au revoir");
                exit;
        end case;
    end loop;
end Check_Direction;