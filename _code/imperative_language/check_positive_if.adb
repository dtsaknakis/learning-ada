-- --------------------------------------------------------
-- Date created  : Aug 27 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses an 'if' statement in Ada2012.
-- Compile       : $ gprbuild check_positive.adb
-- Run           : $ ./check_positive
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO; -- integer I/O

procedure Check_Positive is
    N : Integer;  -- variable declaration
begin
    -- The Get() and Put() procedures are from Integer_Text_IO
    Put ("Enter an integer value: "); -- Put a String
    Get (N); -- Read an integer value (user's input)
    if N > 0 then
        Put (N);  -- Put an Integer
        Put_Line (" is a positive number");
    end if;
end Check_Positive;
