-- --------------------------------------------------------
-- Date created  : Aug 27 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : A hello world program in Ada2012.
-- Compile       : $ gprbuild greet.adb
-- Run           : $ ./greet
-- --------------------------------------------------------

-- 'with' references external modules needed in the procedure
with Ada.Text_IO;  -- std lib module for input/output

-- A procedure does not return a value when called. Greet is the
-- procedure name chosen.
procedure Greet is
begin
    -- Print "Hello, World!" to the screen; use Put_Line() procedure
    Ada.Text_IO.Put_Line ("Hello, World!");
end Greet;
