-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses an 'if/elsif/else' statement in Ada2012.
-- Compile       : $ gprbuild check_direction.adb
-- Run           : $ ./check_direction
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO; -- integer I/O

procedure Check_Direction is
    N : Integer;  -- variable declaration
begin
    -- The Get() and Put() procedures are from Integer_Text_IO
    Put ("Enter an integer value: "); -- Put a String
    Get (N); -- Read an integer value (user's input)
    Put (N);  -- Put an Integer
    if N = 0 or N = 360 then
        Put_Line (" is due east");
    elsif N in 1 .. 89 then
        Put_Line (" is in the northeast quadrant");
    elsif N = 90 then
        Put_Line (" is due north");
    elsif N in 91 .. 179 then
        Put_Line (" is in the northwest quadrant");
    elsif N = 180 then
        Put_Line (" is due west");
    elsif N in 181 .. 269 then
        Put_Line (" is in the southwest quadrant");
    elsif N = 270 then
        Put_Line (" is due south");
    elsif N in 271 .. 359 then
        Put_Line (" is in the southeast quadrant");
    else
        Put_Line (" is not in the range 0 .. 360");
    end if;
end Check_Direction;