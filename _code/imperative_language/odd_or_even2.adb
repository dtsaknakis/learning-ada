-- --------------------------------------------------------
-- Date created  : Aug 28 2019
-- Last update   : -
-- Created by    : Dimitris Tsaknakis
-- Source        : https://learn.adacore.com/index.html
-- Description   : Uses a 'case' expression (note: NOT a case
--               : statement) in a for loop, in Ada2012.
--               : Checks if an integer is odd or even.
-- Compile       : $ gprbuild odd_or_even2.adb
-- Run           : $ ./odd_or_even2
-- --------------------------------------------------------

-- Reference external modules needed in the procedure;
with Ada.Text_IO; use Ada.Text_IO;

procedure Odd_or_Even2 is
begin
    for I in 1 .. 10 loop
        Put_Line (case I is
                  when 1 | 3 | 5 | 7 | 9 => "Odd",
                  when 2 | 4 | 6 | 8 | 10 => "Even");
    end loop;
end Odd_or_Even2;